# Borja de Lope Santos Coliseo

Un juego creado por Borja de Lope Santos.

Desarrollado con Unreal Engine 4.

Toda la documentacion se encuentra en [Doc/deLope_Santos.pdf](./Doc/deLope_Santos.pdf).

En la carpeta [Videos](./Videos/) se encuentra un video de introduccion sobre Unreal Engine 4 y una partida explicando el juego y sus funciones.

## Iniciar el juego
 
 Hay que ir a la capeta Ejecutable Paquete y ejecutar [ColiseoBlue.exe](./Ejecutable&#32;Paquete/ColiseoBlue.exe).

## Controles

| Tecla               | Acción                 | 
| ------------------- | ---------------------- |
| WASD                | Para moverse           |
| E                   | Para hacer dash        |
| Click Izquierdo     | Atacas                 |
| Barra Espaciadora   | Saltar                 |








